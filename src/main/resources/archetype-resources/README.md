# Getting started

To build the lambda function, run
```shell
make build
```

Update ``Makefile`` with the name of the S3 bucket where
we can deploy the function code. To deploy the lambda function, 
run

```shell
make deploy
```

To test that it works, invoke the lambda function
```shell
make invoke
```

To remove the lambda function, run
```shell
make destroy
```
