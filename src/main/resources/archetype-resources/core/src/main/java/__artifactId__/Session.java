#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId};

import java.time.LocalDateTime;

public record Session(
        String id,
        String locationId,
        LocalDateTime localDateTime,
        int availableSpots
) {
}
