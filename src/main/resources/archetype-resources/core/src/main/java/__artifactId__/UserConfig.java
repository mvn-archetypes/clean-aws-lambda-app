#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId};

import java.util.List;

public record UserConfig(
        String userId, List<LocationPreference> preferences
) {
}
