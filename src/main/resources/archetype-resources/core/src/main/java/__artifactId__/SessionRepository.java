#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId};

import java.util.List;

public interface SessionRepository {

    List<Session> findByLocationId(String locationId);

    void bookSession(String sessionId, String userId);
}
