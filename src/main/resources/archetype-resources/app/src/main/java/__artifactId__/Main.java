#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId};

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import lombok.extern.slf4j.Slf4j;
import ${package}.core.BookingService;
import ${package}.core.Session;
import ${package}.core.SessionRepository;
import ${package}.core.UserConfig;

import java.util.Collections;
import java.util.List;

@Slf4j
public class Main implements RequestHandler<Void, String> {

    private final BookingService bookingService;

    public Main() {
        UserConfig config = new UserConfig("foo", Collections.emptyList());
        SessionRepository sessionRepository = new SessionRepository() {
            @Override
            public List<Session> findByLocationId(String locationId) {
                return Collections.emptyList();
            }

            @Override
            public void bookSession(String sessionId, String userId) {
                log.info("Booking session {}", sessionId);
            }
        };
        bookingService = new BookingService(config, sessionRepository);
    }

    @Override
    public String handleRequest(Void unused, Context context) {
        log.info("Starting booking process...");
        bookingService.bookAvailableSessions();
        log.info("Ending booking process...");
        return "Done!";
    }
}
