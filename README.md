This maven archetype defines a skeleton of an application that can be deployed to AWS Lambda.

# Getting started

Clone repository
```shell
git clone git@gitlab.com:mvn-archetypes/clean-aws-lambda-app.git
```
Install archetype
```shell
mvn install
```
Create project from archetype
```shell
mvn archetype:generate -DarchetypeCatalog=local
```

# Deploy to AWS Lambda

Update the S3 bucket name in ``Makefile``

Build project
```shell
mvn clean install
```

Deploy 
```shell
make deploy
```
